package br.edu.up;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServidorRMI {
	
	
	public static void main(String[] args) throws RemoteException {
		
		Calculadora calc = new CalculadoraImpl();
		Registry r = LocateRegistry.createRegistry(9977);
		r.rebind("calcRmi", calc);
		System.out.println("Objeto no repositório!"); 
		
	}

}
